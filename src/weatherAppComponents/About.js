import React from 'react'
import { NavLink } from "react-router-dom"
function About() {
    return (
        <div className="childElement">
            <NavLink to="/" > Home </NavLink>
            <h1>Name: Sameer Trimade</h1>
            <h1>Company: Mountblue Technologies</h1>
            <h1>Position: Trainee</h1>
            <h1>Thought of the day: When something is important enough, you do it even if the odds are not in your favor.</h1>
            <h1>About the project: This is a Weather FOrecast Project. This webapp will show you the weather information based on the client location. You can also search for weather updates of any particular region if you want.</h1>
        </div>

    )
}
export default About