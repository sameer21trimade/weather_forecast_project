import React from 'react'
import { NavLink } from "react-router-dom"
function NavButtons() {    
    return (
        <div>
            <NavLink className="active_class" to="/about" > About </NavLink>        
        </div>
    )
}
export default NavButtons